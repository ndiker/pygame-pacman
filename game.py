#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
import pygame
import random
import os,sys
import math

##CONSTANTS
SCREEN_WIDTH = 800
SCREENT_HRIGHT = 480

SURFACE_WIDTH = 16
SURFACE_HEIGHT = 16

GHOST_START_POINTS = map(lambda x: (x*SURFACE_WIDTH, 13*SURFACE_HEIGHT), range(23, 26))
DIRECTIONS = ['', 'RIGHT', 'LEFT', "UP", "DOWN"]


pygame.display.init()
folder = "." # replace with "." if pictures lay in the same folder as program
try: 
    spritesheet = pygame.image.load(os.path.join(folder, "pacman.png"))
except: 
    raise(UserWarning, "i'm unable to load 'pacman.png' form the folder 'data'") # error msg and exit


screen=pygame.display.set_mode((800,480)) # try out larger values and see what happens !
spritesheet.convert() # convert only works afteer display_setmode is set.
screenrect = screen.get_rect()
background = pygame.Surface((screen.get_size()))
backgroundrect = background.get_rect()
background.fill((0,0,0)) # fill white
background = background.convert()
screen.blit(background,(0,0))


clock = pygame.time.Clock()        #create pygame clock object
mainloop = True
FPS = 60
playtime = 0


SPRITE_MAPPING = {
    'pacman_death': {
        'width': SURFACE_WIDTH,
        'height': SURFACE_HEIGHT,
        'start_point': (3,1),
        'end_point': (14, 1),
    },
    'pacman_dir_right':{
        'width':SURFACE_WIDTH,
        'height':SURFACE_HEIGHT,
        'start_point': (1,1),
        'end_point': (2,1),
    },
    'pacman_dir_left':{
        'width':SURFACE_WIDTH,
        'height':SURFACE_HEIGHT,
        'start_point': (1,2),
        'end_point': (2,2),
    },
    'pacman_dir_up':{
        'width':SURFACE_WIDTH,
        'height':SURFACE_HEIGHT,
        'start_point': (1,3),
        'end_point': (2,3),
    },
    'pacman_dir_down':{
        'width':SURFACE_WIDTH,
        'height':SURFACE_HEIGHT,
        'start_point': (1,4),
        'end_point': (2,4),
    },

    'ghost_red':{
        'width': SURFACE_WIDTH,
        'height': SURFACE_HEIGHT,
        'start_point': (1,5),
        'end_point': (8,5),
    },

    'ghost_pink':{
        'width': SURFACE_WIDTH,
        'height': SURFACE_HEIGHT,
        'start_point': (1,6),
        'end_point': (8,6),
    },

    'ghost_blue':{
        'width': SURFACE_WIDTH,
        'height': SURFACE_HEIGHT,
        'start_point': (1,7),
        'end_point': (8,7),
    },

    'ghost_brown':{
        'width': SURFACE_WIDTH,
        'height': SURFACE_HEIGHT,
        'start_point': (1,8),
        'end_point': (8,8),
    },
    'cherry': (3,2),
    'strawberry': (4,2),
    'peach': (5,2),
    'apple': (6,2),
}


# HELPERS/UTILS

def fuzzyequal(a, b, epsilon):
    return math.fabs(a-b) < epsilon


def get_reverse_direction(direction):
    if direction=='RIGHT': return 'LEFT'
    if direction=='LEFT': return 'RIGHT'
    if direction=='UP': return 'DOWN'
    if direction=='DOWN': return 'UP'

class AnimGameObject(pygame.sprite.Sprite):
    pic_index = 0
    cycletime = 0
    _dx = 0
    _dy = 0

    direction = ''
    next_direction = ''

    collidables = ['x', '_']

    def __init__(self, position, speed, width, height, interval=.15, offsetX=8, offsetY=8):
        pygame.sprite.Sprite.__init__(self)
        self.interval = interval
        self._posX, self._posY = position

        self.width = width
        self.height = height
        self._sX, self._sY = speed

        self._animation = None
        self._current_pic = None

        #collision rect
        self.rect = pygame.Rect(self._posX, self._posY, self.width-offsetX, self.height-offsetY)

        self.offsetX = offsetX
        self.offsetY = offsetY

    def get_surface_list(self, animation_type):
        '''
            creates surface lists for given animation type
            increment posx only 
        '''
        lst = []
    
        surface_width = SPRITE_MAPPING[animation_type]['width']
        surface_height = SPRITE_MAPPING[animation_type]['height']
        
        start_point_x = SPRITE_MAPPING[animation_type]['start_point'][0]
        end_point_x  = SPRITE_MAPPING[animation_type]['end_point'][0]
        point_y = SPRITE_MAPPING[animation_type]['end_point'][1]

        for index in range(start_point_x, end_point_x+1):
            surface = spritesheet.subsurface(
                surface_width * (index-1), surface_height * (point_y-1), surface_width, surface_height)
            surface.set_colorkey((0,0,0))
            surface.convert_alpha()
            lst.append(surface)
        return lst

    def animate_sprite_list(self, animation_list, seconds):
            self.cycletime += seconds
            if self.cycletime > self.interval:
                self._current_pic = animation_list[self.pic_index]
                self.pic_index += 1
                if self.pic_index > len(animation_list)-1:
                    self.pic_index = 0
                self.cycletime = 0

    @property
    def animation_name(self):
        return self._animation

    @animation_name.setter
    def animation_name(self, value):
        self._animation = value                

    @property
    def current_pic(self):
        return self._current_pic

    @current_pic.setter
    def current_pic(self, value):
        self._current_pic = value

    @property
    def position(self):
        return (self._posX, self._posY)

    @position.setter
    def position(self, value):
        self._posX = value[0]
        self._posY = value[1]

    @property
    def acceleration(self):
        return (self._dx, self._dy)

    @acceleration.setter
    def acceleration(self, value):
        self._dx = value[0]
        self._dy = value[1]

    def draw(self):
        screen.blit(self._current_pic, (self._posX,self._posY))

    def update(self):
        pass

    def play_animation(self):
        pass

    def position_to_point(self):
        px = round(self._posX/SURFACE_WIDTH, 0)
        py = round(self._posY/SURFACE_HEIGHT, 0)
        return int(px), int(py)

    def get_rectangle(self):
        return pygame.Rect(*self.rect)

    def change_direction(self, direction_key):
        tile_val, tile_rect = self.get_next_tile(direction_key)
        
        px,py = self.position_to_point()

        if self.direction:
            # can object turn ? 
            tpx = fuzzyequal(tile_rect[0], math.floor(self._posX), 3)
            tpy = fuzzyequal(tile_rect[1], math.floor(self._posY), 3)
        else:
            tpx = True
            tpy = True

        if tile_val not in self.collidables:
            if direction_key == 'RIGHT' and tpy:
                self.acceleration = (1, 0)
                self.animation_name = 'animation_right'
                self.direction = 'RIGHT'

            if direction_key == 'LEFT' and tpy:
                self.acceleration = (-1, 0)
                self.animation_name = 'animation_left'
                self.direction = 'LEFT'
                
            if direction_key == 'UP'  and tpx:
                self.acceleration = (0,-1)
                self.animation_name = 'animation_up'
                self.direction = 'UP'
                
            if direction_key == 'DOWN' and tpx:
                self.acceleration = (0, 1)
                self.animation_name = 'animation_down'
                self.direction = 'DOWN'
                


    def get_next_tile(self, direction_key):
        adjacent_tiles = Map.get_adjacent_tiles(self.position_to_point())
        return adjacent_tiles[direction_key]

    def get_direction(self):
        if self.acceleration == (1,0):
            return 'RIGHT'
        if self.acceleration == (-1,0):
            return 'LEFT'
        if self.acceleration == (0,1):
            return 'DOWN'
        if self.acceleration == (0, -1):
            return 'UP'

    def check_wall_collision(self):
        # direction = self.get_direction()
        
        tile_val, tile_rect = self.get_next_tile(self.direction)

        if self.direction == 'RIGHT':
            p = pygame.Rect(self.rect[0]+self.width-3, self.rect[1], self.rect[2], self.rect[3])
        elif self.direction == 'DOWN':
            p = pygame.Rect(self.rect[0], self.rect[1]+self.height-3, self.rect[2], self.rect[3])
        elif self.direction == 'LEFT':
            p = pygame.Rect(self.rect[0]+1, self.rect[1], self.rect[2], self.rect[3])
        else:
            p=pygame.Rect(*self.rect)
        
        w = pygame.Rect(*tile_rect)
        
        if tile_val == 'x' and p.colliderect(w):
            return True
        return False

    def move(self, seconds):
        self._posX += self._dx * self._sX * seconds
        self._posY += self._dy * self._sY * seconds

    def stop(self):
        self.acceleration = (0, 0)


class PacMan(AnimGameObject):

    def __init__(self, position, speed, width=SURFACE_WIDTH, height=SURFACE_HEIGHT, interval=.15, offsetX=9, offsetY=9):
        AnimGameObject.__init__(self, position, speed, width, height, interval, offsetX, offsetY)

        # specify animations
        self.animation_death = self.get_surface_list('pacman_death')
        self.animation_right = self.get_surface_list('pacman_dir_right')
        self.animation_left = self.get_surface_list('pacman_dir_left')
        self.animation_up = self.get_surface_list('pacman_dir_up')
        self.animation_down = self.get_surface_list('pacman_dir_down')

        self.current_pic = self.animation_right[0] # default anim
        self._animation = 'animation_right'
        
        self.direction = 'RIGHT'
        self.next_direction = 'RIGHT'
        self.acceleration = (1,0)

    def play_animation(self, seconds):
        if self.animation_name == 'animation_right':
            self.animate_sprite_list(self.animation_right, seconds)

        elif self.animation_name == 'animation_left':
            self.animate_sprite_list(self.animation_left, seconds)

        elif self.animation_name == 'animation_up':
            self.animate_sprite_list(self.animation_up, seconds)

        elif self.animation_name == 'animation_down':
            self.animate_sprite_list(self.animation_down, seconds)


    def check_dot_collision(self):
        _dot_rects = map(lambda x: x.rect, the_map._dots)
        _pac_col_rect = pygame.Rect(self.rect[0], self.rect[1], self.rect[2], self.rect[3])


        dot_index = _pac_col_rect.collidelist(_dot_rects)
        
        if dot_index > -1:
            dot = the_map._dots[dot_index]
            if dot.__class__.__name__ == 'PowerUp':
                print('stronger') 
            del the_map._dots[dot_index]

    def update(self, seconds):
        self.change_direction(self.next_direction)
        self.move(seconds)
        self.play_animation(seconds)
        self.rect.centerx = round(self._posX, 0)
        self.rect.centery = round(self._posY, 0)

        self.check_dot_collision()

        if self.acceleration != (0,0):
            if self.check_wall_collision():
                self.stop()
        
class Ghost(AnimGameObject):
    def __init__(self, ghost_name, position, speed, width=SURFACE_WIDTH, height=SURFACE_HEIGHT, interval=.15, offsetX=9, offsetY=9):
        AnimGameObject.__init__(self, position, speed, width, height, interval, offsetX, offsetY)
        self.animation_idle = self.get_surface_list(ghost_name)
        self.current_pic = self.animation_idle[0]
        self._animation = 'ghost_anim'
        self.collidables = ['x']


    def play_animation(self, seconds):
        # if self.animation_name == 'ghost_anim':
        self.animate_sprite_list(self.animation_idle, seconds)

    def get_random_direction(self):
        direction = DIRECTIONS[random.randint(0, len(DIRECTIONS)-1)]
        if not (self.direction == direction or direction == ''):
            return direction
        return self.get_random_direction()

    def ghost_in_intersection(self):
        adjacent_tiles = Map.get_adjacent_tiles(self.position_to_point())
        return len(filter(lambda tile_info: tile_info[0] not in self.collidables, adjacent_tiles.values())) > 1

    def choose_direction(self):
        adjacent_tiles = Map.get_adjacent_tiles(self.position_to_point())

        available_directions = []

        #collecting available directions
        for direction, tile_info in adjacent_tiles.items():

            # if ghost is in the ghost house and door is in UP direction,
            # ghost won't change the direction
            if direction == 'UP' and tile_info[0] == '_':
                return direction

            # when ghost manages to escape, we add door value into collidables
            if direction not in ['', get_reverse_direction(self.direction)] and tile_info[0] not in self.collidables + ['_']:
                available_directions.append(direction)

        # rev = get_reverse_direction(self.direction)
        # if rev in available_directions:
        #     available_directions.remove(rev)

        if len(available_directions) > 0 or self.acceleration == (0,0):
            ## choose randomly
            return available_directions[random.randint(0, len(available_directions)-1)]
            
        # print(available_directions)
        return self.next_direction


    def update(self, seconds):
        self.play_animation(seconds)
        self.rect.centerx = round(self._posX, 0)
        self.rect.centery = round(self._posY, 0)

        # if self.acceleration != (0,0) and self.check_wall_collision():
        #     self.stop()

        #choose direction
        self.next_direction = self.choose_direction()

        print(self.direction)

        # change direction
        if self.next_direction != '':
            self.change_direction(self.next_direction)

        # xx = self.ghost_in_intersection()
        # print(xx)
        self.move(seconds)
        

class StaticGameObject(pygame.sprite.Sprite):

    def __init__(self, position, width, height):
        super(StaticGameObject, self).__init__()
        self._posX, self._posY = position
        self.width = SURFACE_WIDTH
        self.height = SURFACE_HEIGHT

        self.rect = pygame.Rect(self._posX, self._posY, self.width, self.height)


    @staticmethod
    def factory(type, **kwargs):
        if type == 'Fruit': return Fruit(**kwargs)
        if type == 'Wall': return Wall(**kwargs)
        if type == 'Dot': return Dot(**kwargs)
        if type == 'PowerUp': return PowerUp(**kwargs)
        assert 0, 'Object Type Not Recognized'

    @property
    def position(self):
        return (self._posX, self._posY)

    @position.setter
    def position(self, value):
        self._posX = value[0]
        self._posY = value[1]

    def update(self, seconds):
        pass

    def draw(self):
        screen.blit(self.surface, (self._posX, self._posY))

class Fruit(StaticGameObject):

    def __init__(self, position, name, score, width=SURFACE_WIDTH, height=SURFACE_HEIGHT):
        super(Fruit, self).__init__(position, width, height)
        
        self.score = score
        self._posX, self._posY = position
       
        self._point_x, self._point_y = SPRITE_MAPPING[name]

        self.surface = spritesheet.subsurface(
                self.width * (self._point_x-1), self.height * (self._point_y-1), self.width, self.height)
        self.surface.set_colorkey((0,0,0))
        self.surface.convert_alpha()

class Dot(StaticGameObject):
    def __init__(self, position, score=10, width=SURFACE_WIDTH, height=SURFACE_HEIGHT, radius=2):
        super(Dot, self).__init__(position, width, height)
        
        self.score = score
        self.position = (position[0] + int(width/2), position[1] + int(height/2) )
        self.color = (255,255,255)
        self.radius = radius
        self.rect = pygame.Rect(position[0], position[1], width, height)
        

    def draw(self):
        pygame.draw.circle(screen, self.color, self.position, self.radius)


class PowerUp(Dot):

    def __init__(self, *args, **kwargs):
        super(PowerUp, self).__init__(*args, **kwargs)
        self.radius = 4
        self.color = (255, 0, 0)

class Wall(StaticGameObject):
    
    def __init__(self, position, color=(0, 0, 255), width=SURFACE_WIDTH, height=SURFACE_HEIGHT):
        super(Wall, self).__init__(position, width, height)
        self.color = color

    def draw(self):
        pygame.draw.rect(background, self.color, (self._posX, self._posY, self.width-1, self.height-1))


class Map():

    map = [
    'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    'xp......................x.......................px',
    'x.xxxxxxxxxx.xxxxxxxxxx.x.xxxxxxxxxx.xxxxxxxxxxx.x',
    'x.x        x.x        x.x.x        x.x         x.x',
    'x.x        x.x        x.x.x        x.x         x.x',
    'x.xxxxxxxxxx.xxxxxxxxxx.x.xxxxxxxxxx.xxxxxxxxxxx.x',
    'x................................................x',
    'x.xxxxxxxxxx.x.xxxxxxxxxxxxxxxxxxxxxx.x.xxxxxxxx.x',
    'x.xxxxxxxxxx.x.xxxxxxxxxxxxxxxxxxxxxx.x.xxxxxxxx.x',
    'xp...........x....xxxxxxxxxxxxxxxx....x.........px',
    'xxxxxxxxxxxx.xxxx.xxxxxxxxxxxxxxxx.xxxx.xxxxxxxxxx',
    '           x.x........................x.x         ',
    '           x.x.xxxxxxxxxx___xxxxxxxxx.x.x         ',
    '           x.x.xxxxxxxx       xxxxxxx.x.x         ',
    'xxxxxxxxxxxx.x.xxxxxxxxxxxxxxxxxxxxxx.x.xxxxxxxxxx',
    '...............xxxxxxxxxxxxxxxxxxxxxx.............',
    'xxxxxxxxxxxx.x.xxxxxxxxxxxxxxxxxxxxxx.x.xxxxxxxxxx',
    '           x.x. ......................x.x         ',
    '           x.x.xxxxxxxxxxxxxxxxxxxxxx.x.x         ',
    '           x.x.xxxxxxxxxxxxxxxxxxxxxx.x.x         ',
    'xxxxxxxxxxxx.x.xxxxxxxxxxxxxxxxxxxxxx.x.xxxxxxxxxx',    
    'xp.................xxxxxxxxxxxxxx...............px',
    'x.xxxxxxxxxx.xxxxx.xxxxxxxxxxxxxx.xxxxx.xxxxxxxx.x',
    'xp.........x............................x.......px',
    'xxxxxxxxxx.x.x.xxxxxxxxxxxxxxxxxxxxxx.x.x.xxxxxxxx',
    'xxxxxxxxxx.x.x.xxxxxxxxxxxxxxxxxxxxxx.x.x.xxxxxxxx',
    'xp...........x.....xxxxxxxxxxxxxx.....x.........px',
    'x.xxxxxxxxxxxxxxxx.xxxxxxxxxxxxxx.xxxxxxxxxxxxxx.x',
    'xp..............................................px',
    'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    ]

    wall = StaticGameObject.factory(type='Wall', position=(0, 0))
    door = StaticGameObject.factory(type='Wall', position=(0, 0), color=(255,0,0))

    _wall_positions = []
    _dots = []
    _door_positions = []

    def __init__(self):
        for y in range(len(self.map)): 
            for x in range(len(self.map[0])):
                val = self.map[y][x]
                if val == 'x':
                    self._wall_positions.append((x*SURFACE_WIDTH, y*SURFACE_HEIGHT))
                elif val == '.':
                    dot = StaticGameObject.factory(type='Dot', position=((x*SURFACE_WIDTH), (y*SURFACE_HEIGHT)))
                    self._dots.append(dot)
                elif val == 'p':
                    powerup = StaticGameObject.factory(type='PowerUp', position=((x*SURFACE_WIDTH), (y*SURFACE_HEIGHT)))
                    self._dots.append(powerup)
                elif val == '_':
                    self._door_positions.append((x*SURFACE_WIDTH, y*SURFACE_HEIGHT))

    def draw(self):
        for wall_p in self._wall_positions:
            self.wall.position = wall_p
            self.wall.draw()

        for door_p in self._door_positions:
            self.door.position = door_p
            self.door.draw()

        for dot in self._dots:
            dot.draw()


    @classmethod
    def get_adjacent_tiles(self, point):
        ''' returns adjacent tiles data'''

        px, py = point

        # returns (tile string value, tile rectangle)
        get_tile_info = lambda x,y: (self.map[y][x] , (x * self.wall.width, y* self.wall.height, self.wall.width, self.wall.height))

        return {
            '': get_tile_info(px, py),
            'UP': get_tile_info(px, py-1),
            'DOWN':get_tile_info(px, py+1),
            'RIGHT':get_tile_info(px+1, py),
            'LEFT':get_tile_info(px-1, py),
        }

### OBJECTS

pacman = PacMan(position=(400,273), speed=(80,80))

red_ghost = Ghost(position=GHOST_START_POINTS[0], speed=(50,50), ghost_name='ghost_red')
pink_ghost = Ghost(position=(150, 100), speed=(20,20), ghost_name='ghost_pink')
blue_ghost = Ghost(position=(200, 100), speed=(20,20), ghost_name='ghost_blue')
brown_ghost = Ghost(position=(250, 100), speed=(20,20), ghost_name='ghost_brown')

cherry = StaticGameObject.factory(type='Fruit', position=(350, 100), name='cherry', score=100)
strawberry = StaticGameObject.factory(type='Fruit', position=(400, 100), name='strawberry', score=120)
peach = StaticGameObject.factory(type='Fruit', position=(450, 100), name='peach', score=140)
apple = StaticGameObject.factory(type='Fruit', position=(500, 100), name='apple', score=200)

fruits = pygame.sprite.Group()
fruits.add(cherry)
fruits.add(strawberry)
fruits.add(peach)
fruits.add(apple)

ghosts = pygame.sprite.Group()
ghosts.add(red_ghost)
# ghosts.add(pink_ghost)
# ghosts.add(blue_ghost)
# ghosts.add(brown_ghost)

the_map = Map()

#####

while mainloop:
    screen.blit(background, (0,0))  
    milliseconds = clock.tick(FPS)  # milliseconds passed since last frame
    seconds = milliseconds / 1000.0 # seconds passed since last frame (float)
    playtime += seconds    
    pressed_keys = pygame.key.get_pressed()

    # KEYS
    if pressed_keys[pygame.K_RIGHT]:
        pacman.next_direction = 'RIGHT'

    if pressed_keys[pygame.K_LEFT]:
        pacman.next_direction = 'LEFT'

    if pressed_keys[pygame.K_UP]:
        pacman.next_direction = 'UP'

    if pressed_keys[pygame.K_DOWN]:
        pacman.next_direction = 'DOWN'

    if pressed_keys[pygame.K_SPACE]:
        pacman.stop()
        pacman.next_direction = ''

    ##QUIT
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            mainloop = False # pygame window closed by user
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                mainloop = False # user pressed ESC

    the_map.draw()
    pacman.draw()
    pacman.update(seconds)

    for ghost in ghosts:
        ghost.draw()
        ghost.update(seconds)

    # for fruit in fruits:
    #     fruit.draw()

 

    
    # print(pygame.sprite.collide_rect(pacman, cherry))
    # collided = pygame.sprite.spritecollide(pacman, fruits, True)



            # screen.blit(background.subsurface((x*)), ())


    pygame.display.flip()          # flip the screen 30 times a second

pygame.quit()
# sys.exit()
